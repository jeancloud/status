# Statuts de l’association Jean-Cloud
## Article 1 - Constitution
Il est fondé entre les adhérents aux présents statuts une association régie par la loi du 1er juillet 1901 et le décret du 16 août 1901 ayant pour titre : « Jean-Cloud »

## Article 2 - Objet
Fournir des services numériques.
Sensibiliser aux impacts sociaux, sociétaux et environnementaux du numérique ainsi que sur la non-neutralité des technologies.
Concevoir et/ou héberger des solutions techniques répondant à ces problèmes.
Organiser des rencontres, ateliers thématiques, formations ou tout ce qui semblera en cohérence avec
son projet.
Encourager les actions rassemblant des acteurs locaux, concernés par l’activité de l’association.

## Article 3 - Siège Social
Le siège social est :
1 Allée d’Athena
69100 Villeurbanne
Il pourra être transféré par simple décision du Collège et ratification par une Assemblée Générale
extraordinaire.

## Article 4 – Composition
### Article 4 a – Membres de l’association
L’association se compose de membres actifs, et de membres adhérents.
Les membres actifs sont les seuls à posséder une voix lors des votes aux Assemblées Générales.
Les membres adhérents reçoivent les informations de l’association via la newsletter et tout autre moyen
dont l’association dispose.

### Article 4 b – Modalités d’inscription
Est membre actif, toute personne prenant part aux activités de l’association et présentant une demande
d’admission acceptée par vote par le Collège, ou qui était membre actif l’année passée et renouvelle son
inscription. Ils doivent signer et approuver le Règlement Intérieur.
Est membre adhérent, toute personne en faisant la demande au Collège.
Aucun frais d’inscription ne peut être demandé aux membres, actifs ou adhérents.

## Article 5 – Assemblées Générales
### Article 5 a – Assemblée Générale ordinaire
L'Assemblée Générale ordinaire est ouverte à tous les membres de l'association. L’AG ordinaire
concerne le déroulement courant de l’association.
L’AG ordinaire se rassemble et est organisée selon les termes du Règlement Intérieur.

### Article 5 b – Assemblée Générale extraordinaire
L’Assemblée Générale extraordinaire se réunit pour
1. L’élection du Collège,
2. La radiation d’un membre,
3. Un changement dans les statuts,
4. Un changement du Règlement Intérieur,
5. La révocation du Collège,
6. La dissolution de l’association,

L’AG extraordinaire se réunit à l’appel du Collège, ou d’au moins un tiers des membres actifs ; la liste
de ces membres et l’appel sont rendus disponibles auprès de tous les membres.
Lorsque l’appel est formulé, le secrétaire fixe une date de tenue de l’AG extraordinaire, entre 15 et 30
jours suivant l’appel et transmet l’information à l’ensemble des membres actifs. Les membres actifs
doivent être prévenus un minimum de 14 jours à l’avance de la tenue d’une AG extraordinaire.
Si l’AG extraordinaire concerne une élection, un changement de statuts ou du Règlement Intérieur, ou la
dissolution de l’association, le secrétaire est le seul organisateur.
Si l’AG extraordinaire concerne toute autre question, le secrétaire s’adjoindra d’au moins une autre
personne pour organiser l’AG, selon les modalités précisées dans le Règlement Intérieur.

Article 5 c – La prise de décision
Les décisions concernant l’association, qu’elles soient prises pendant les AGs ordinaires ou
extraordinaire, suivent les modalités inscrites dans le Règlement Intérieur.
Celui-ci doit stipuler la méthode de prise de décision, selon la nature de ladite décision, le ou les
organisateurs du vote ainsi que le ou les responsables de la justesse du résultat. De plus, le Règlement
Intérieur précise les durées et objectifs de chaque phase de la prise de décision, soit avant, pendant et
après celle-ci.

Article 6 - Radiation
La qualité de membre se perd par :
a) la démission,
b) le décès,
c) la radiation, prononcée après vote de l'Assemblée Générale extraordinaire comme précisé
dans le Règlement Intérieur,
d) pour les membres actifs, par le non renouvellement de leur inscription annuelle.

Article 7 – Ressources
Les ressources de l'association comprennent :
- 
- toutes autres ressources autorisées par la loi.

Article 9
Article 9 a – Le Collège
L’association est tenue par un Collège composé de deux ou trois membres actifs, qui sont alors tous coprésidents. Le Collège a la charge de l’administration de l’association. En particulier, il est responsable de la pérennité légale de l’association.
Le Collège est également responsable du compte bancaire et des finances de l’association. Il fait appliquer les statuts et le Règlement Intérieur de l’association.
Le Collège est habilité à parler au nom de l’association en toutes circonstances.
Les membres du Collège sont tous responsables de ces tâches.
De plus, toute décision du Collège concernant ses domaines de responsabilité est prise collectivement par les trois co-présidents.
Ils sont également solidairement responsables de l’association devant la Loi.
Ainsi, en cas de poursuites judiciaires, les membres du Collège en place au moment des faits répondront collectivement et
solidairement de leurs responsabilités devant les tribunaux compétents.
Les membres du Collège sont élus un à un selon les modalités inscrites dans le Règlement Intérieur. Ils
sont élus pour un mandat d’un an.
Le mandat des membres du Collège est renouvelable.
Les membres du Collège sont révocables, selon les modalités inscrites dans le Règlement Intérieur.
Si un membre du Collège venait à manquer en cours de mandat, les autres membres peuvent soit
nommer quelqu’un pour le remplacer, soit pourvoir à son poste en attendant la tenue de prochaines
élections.
Pour finir, les membres du Collège sortant s’assurent de la passation pour le nouveau Collège, de façon
à ce que celui-ci soit prêt à prendre la relève. Notamment, les contacts, calendriers, état financier,
méthodes et plus généralement la vie courante de l’association seront partagés avec les nouveaux
membres du Collège ; tant que faire se peut, les anciens membres accompagneront les nouveaux lors
d’une période de transition de quelques semaines.

Article 9 b – Le secrétaire
Le secrétaire est élu en même temps que le Collège pour un mandat d’un an. Il est responsable du bon
déroulement des Assemblées Générales extraordinaires. Conformément à l'article 5 b portant sur les
Assemblées Générales extraordinaires, le secrétaire n'a pas de légitimité à convoquer une AG
extraordinaire de sa propre initiative.
Le secrétaire est révocable, selon les modalités inscrites dans le Règlement Intérieur.

Le secrétaire est également responsable de la liste des membres adhérents et actifs. Elle doit rester à jour
et respecter les termes de l’Article 4. Le secrétaire la laisse à disposition des membres actifs.
Si l’association ne compte pas suffisamment de membres, le rôle du secrétaire pourra être assuré par le
Collège.

Article 10 – Règlement Intérieur
Le Règlement Intérieur est mis en place par le Collège et approuvé par vote lors d’une Assemblée
Générale.
Il est destiné à fixer divers points de la gestion interne de l’association. Il est établi pour permettre,
chaque année, de modifier son fonctionnement pour être le plus en accord avec la volonté des membres
de l’association.
Plus précisément, le Règlement Intérieur statuera a minima sur les points suivants :
1. L’organisation des Assemblée Générale ordinaires,
2. La communication extérieure de l’association,
4. L’organisation des débats et discussions,
5. La révocation d’un membre élu, ainsi que la radiation d’un membre actif,
6. Les mécanismes de prise de décision et d’élection, avec les informations précisées dans
l’Article 5 c.
Le Règlement Intérieur se modifie à la demande d’un tiers des membres actifs, demande inscrite à
l’ordre du jour d’une AG ordinaire. Cette demande consiste en une justification de la ou des
modifications, et en une proposition de correction du Règlement Intérieur.
Cette proposition est ensuite discutée en groupes restreints n’excédant pas 6 personnes, qui proposeront
des modifications en lien avec la demande formulée. Le secrétaire recevra les différentes propositions
rédigées en lien avec la modification initiale jusqu'à une date fixée par le Règlement Intérieur, d'au
minimum trois jours avant une AG extraordinaire organisée pour l’occasion.
Les différentes propositions rédigées seront listées, portées à la connaissance de tous les membres, puis
soumises au débat hors de l’AG extraordinaire pendant une durée d’une semaine au moins.
La décision finale sera prise selon les modalités inscrites dans le précédent Règlement Intérieur.
La décision finale sera mise en oeuvre par le Collège.
Tout article du Règlement Intérieur doit être cohérent avec les statuts de l’association. En cas de conflit,
les statuts prévalent toujours sur le Règlement Intérieur.

Article 11 – Dissolution
En cas de dissolution prononcée par les deux tiers au moins des membres présents à l’Assemblée
Générale Extraordinaire, un ou plusieurs liquidateurs sont nommés par celle-ci et l’actif, s’il y a lieu, est
dévolu conformément à l’article 9 de la loi du 1 er juillet 1901 et au décret du 16 août 1901.

Adrian Amaglio, membre du Collège
Théophile Lemarié, membre du Collège
